import canAttackEachOther from '../src/queen-attack';
import ChessCoordinates from '../src/chess-coordinates';

describe("Queen attack behavior", () => {
    it("Queens that cannot attack", () => {
        expect(
            canAttackEachOther(
                new ChessCoordinates(2, 4),
                new ChessCoordinates(6, 6)
            )
        ).toBe(false);

        expect(
            canAttackEachOther(
                new ChessCoordinates(0, 4),
                new ChessCoordinates(6, 1)
            )
        ).toBe(false);
    });
    
    it("Queens on the same row can attack", () => {
        expect(
            canAttackEachOther(
                new ChessCoordinates(2, 4),
                new ChessCoordinates(2, 6)
            )
        ).toBe(true);
    });

    it("Queens on the same column can attack", () => {
        expect(
            canAttackEachOther(
                new ChessCoordinates(2, 4),
                new ChessCoordinates(6, 4)
            )
        ).toBe(true);
    });

    it("Queens in diagonal can attack", () => {
        expect(
            canAttackEachOther(
                new ChessCoordinates(2, 2),
                new ChessCoordinates(0, 4)
            )
        ).toBe(true);

        expect(
            canAttackEachOther(
                new ChessCoordinates(2, 2),
                new ChessCoordinates(3, 1)
            )
        ).toBe(true);

        expect(
            canAttackEachOther(
                new ChessCoordinates(2, 2),
                new ChessCoordinates(1, 1)
            )
        ).toBe(true);

        expect(
            canAttackEachOther(
                new ChessCoordinates(2, 2),
                new ChessCoordinates(5, 5)
            )
        ).toBe(true);
    });
});