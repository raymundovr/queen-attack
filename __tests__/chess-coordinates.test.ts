import ChessCoordinates from '../src/chess-coordinates';

describe("Chess coordinates behavior", () => {
    it("Return a coordinates object when rank and file are on board (between 0 and 8)", () => {        
        expect(
            new ChessCoordinates(2, 4)
        ).toEqual({
            row: 2,
            column: 4,
        });
    });

    it("Throw an error when coordinates are off board", () => {        
        expect(
            () => new ChessCoordinates(-1, 2)
        ).toThrow();
        expect(
            () => new ChessCoordinates(8, 2)
        ).toThrow();
        expect(
            () => new ChessCoordinates(5, -1)
        ).toThrow();
        expect(
            () => new ChessCoordinates(5, 8)
        ).toThrow();
    });
});