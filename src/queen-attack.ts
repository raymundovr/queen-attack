/**
 * Task:
 * Given 2 queens on a chess board, write a function that tells me if the queens can attack each other
 * So function signature would be:
 * can_attack_each_other(queen_1: QueenCoordinatesType, queen_2: QueenCoordinatesType) => boolean
*/

import ChessCoordinates from './chess-coordinates';

function canAttackEachOther(queen1: ChessCoordinates, queen2: ChessCoordinates) : boolean {
    const rowDiff = Math.abs(queen1.row - queen2.row);
    const colDiff = Math.abs(queen1.column - queen2.column);

    return rowDiff === 0 || colDiff === 0 || rowDiff === colDiff;
}

export default canAttackEachOther;