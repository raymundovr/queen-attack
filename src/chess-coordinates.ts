export default class ChessCoordinates {
    public row: number;
    public column: number;
    
    constructor(row, column) {
        if (!ChessCoordinates.isValidCoordinate(row) || !ChessCoordinates.isValidCoordinate(column)) {
            throw new Error("Invalid coordinates");
        }

        this.row = row;
        this.column = column;
    }

    static isValidCoordinate (coordinate: number): boolean {
        return coordinate >= 0 && coordinate < 8;
    }
}