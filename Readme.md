# Queen attack

## Description

Given two queens in a chess board determine if the queens can attack each other.

## Run
To run the tests

1. Clone this repository.
2. Navigate to the downloaded folder.
3. Run ```npm install```.
4. Run ```npm test```.